package com.qbb.mybatisplus;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author QIUQIU&LL (个人博客:https://www.cnblogs.com/qbbit)
 * @date 2022-11-18  21:48
 * @tags 我爱的人在很远的地方, 我必须更加努力
 */
public class MappingTest {
    public static void main(String[] args) {
        /**
         * 利用java8 lambdas语言特性处理如下java集合对象，
         * 为了方便描述对象用json表示为[{id:1,name:null},{id:1,name:”liwei”},{id:2,name:”zhansan”},{id:2,name:”lisi”}]
         * 期望处理后的集合对象为[{id:1,name:[liwei]},{id:2, name:[zhansan,lisi] }]
         */
        List<Person> personList = Arrays.asList(
                new Person(1),
                new Person(1, "liwei"),
                new Person(2, "zhansan"),
                new Person(2, "lisi")
        );
        // List<String> list = personList.stream().collect(Collectors.mapping(Person::getName, Collectors.toList()));
        Map<Integer, List<String>> listMap = personList.stream().filter(e -> StringUtils.isNotBlank(e.getName())).collect(Collectors.groupingBy(Person::getId, Collectors.mapping(Person::getName, Collectors.toList())));
        System.out.println("listMap = " + listMap);
    }
}
