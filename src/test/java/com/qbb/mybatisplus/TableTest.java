package com.qbb.mybatisplus;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.google.common.collect.Tables;

import java.util.Map;
import java.util.Set;

/**
 * @author QIUQIU&LL (个人博客:https://www.cnblogs.com/qbbit)
 * @date 2022-11-18  21:48
 * @tags 我爱的人在很远的地方, 我必须更加努力
 */
public class TableTest {
    public static void main(String[] args) {
        Table<String, String, Integer> tables = HashBasedTable.create();
        // 测试数据  学生 --- 课程 --- 分数
        tables.put("张三", "javase", 80);
        tables.put("李四", "javase", 90);
        tables.put("张三", "oracle", 100);
        tables.put("王五", "oracle", 95);
        tables.put("王五", "Go", 60);
        tables.put("秋秋", "Go", 100);
        // 所有的行数据
        Set<Table.Cell<String, String, Integer>> cells = tables.cellSet();
        for (Table.Cell<String, String, Integer> temp : cells) {
            System.out.println(temp.getRowKey() + ":" + temp.getColumnKey() + ":" + temp.getValue());
        }
        // List<Table.Cell<String, String, Integer>> list = cells.stream().sorted((o1, o2) -> o1.getValue() - o2.getValue()).collect(Collectors.toList());
        // System.out.println("list = " + list);
        /**
         * 张三:javase:80
         * 张三:oracle:100
         * 李四:javase:90
         * 王五:oracle:95
         * 王五:Go:60
         * 秋秋:Go:100
         */
        System.out.println("==========学生查看成绩==============");
        System.out.print("学生\t");
        // 所有的课程
        Set<String> cours = tables.columnKeySet();
        for (String t : cours) {
            System.out.print(t + "\t");
        }
        System.out.println();
        /**
         * 学生	javase	oracle	Go
         */
        // 所有的学生
        Set<String> stus = tables.rowKeySet();
        for (String stu : stus) {
            System.out.print(stu + "\t");
            Map<String, Integer> scores = tables.row(stu);
            for (String c : cours) {
                System.out.print(scores.get(c) + "\t");
            }
            System.out.println();
        }
        /**
         * 学生	javase	oracle	Go
         * 张三	80	    100	    null
         * 李四	90	    null	null
         * 王五	null	95	    60
         * 秋秋	null	null	100
         */
        System.out.println("==========课程查看成绩==============");
        System.out.print("课程\t");
        // 所有的学生
        Set<String> stuSet = tables.rowKeySet();
        for (String t : stuSet) {
            System.out.print(t + "\t");
        }
        /**
         * 	张三	李四	王五	秋秋
         */
        System.out.println();
        // 所有的课程
        Set<String> courSet = tables.columnKeySet();
        for (String c : courSet) {
            System.out.print(c + "\t");
            Map<String, Integer> scores = tables.column(c);
            for (String s : stuSet) {
                System.out.print(scores.get(s) + "\t");
            }
            System.out.println();
        }
        /**
         * 课程	    张三	    李四	    王五	    秋秋
         * javase	80	    90	    null	null
         * oracle	100	    null	95	    null
         * Go	    null	null	60	    100
         */

        System.out.println("===========转换===========");
        // 相当于把rowKey和columnKey做了一个交换
        Table<String, String, Integer> tables2 = Tables.transpose(tables);
        // 所有的行数据
        Set<Table.Cell<String, String, Integer>> cells2 = tables2.cellSet();
        for (Table.Cell<String, String, Integer> temp : cells2) {
            System.out.println(temp.getRowKey() + ":" + temp.getColumnKey() + ":" + temp.getValue());
        }
        /**
         * javase:张三:80
         * oracle:张三:100
         * javase:李四:90
         * oracle:王五:95
         * Go:王五:60
         * Go:秋秋:100
         */

        main2();
    }

    public static void main2() {
        Table<String, String, String> table = HashBasedTable.create();
        // 使用员工详细信息初始化表
        table.put("IBM", "101", "Mahesh");
        table.put("IBM", "102", "Ramesh");
        table.put("IBM", "103", "Suresh");

        table.put("Microsoft", "111", "Sohan");
        table.put("Microsoft", "112", "Mohan");
        table.put("Microsoft", "113", "Rohan");

        table.put("TCS", "121", "Ram");
        table.put("TCS", "122", "Shyam");
        table.put("TCS", "123", "Sunil");

        // 获取与IBM对应的Map
        Map<String, String> ibmMap = table.row("IBM");

        System.out.println("===========IBM员工名单===========");
        for (Map.Entry<String, String> entry : ibmMap.entrySet()) {
            System.out.println("Emp Id: " + entry.getKey() + ", Name: " + entry.getValue());
        }
        System.out.println();

        // 获取表格的所有唯一键
        System.out.println("===========获取表格的所有唯一键===========");
        Set<String> employers = table.rowKeySet();
        System.out.print("Employers: ");
        employers.forEach(e -> System.out.print(e + " "));
        System.out.println("\n");

        // 得到一个对应102的Map
        System.out.println("===========得到一个对应102的Map===========");
        Map<String, String> EmployerMap = table.column("102");
        for (Map.Entry<String, String> entry : EmployerMap.entrySet()) {
            System.out.println("Employer: " + entry.getKey() + ", Name: " + entry.getValue());
        }

        main3();
    }

    public static void main3() {
        System.out.println("===========main3()===========");
        Table<String, Integer, Integer> table = HashBasedTable.create();
        table.put("A", 1, 100);
        table.put("A", 2, 101);
        table.put("B", 1, 200);
        table.put("B", 2, 201);

        /**
         *  contains(Object rowKey, Object columnKey)：
         *  Table中是否存在指定rowKey和columnKey的映射关系
         */
        boolean containsA3 = table.contains("A", 3); // false
        System.out.println("containsA3 = " + containsA3);
        boolean containColumn2 = table.containsColumn(2); // true
        System.out.println("containColumn2 = " + containColumn2);
        boolean containsRowA = table.containsRow("A");  // true
        System.out.println("containsRowA = " + containsRowA);
        boolean contains201 = table.containsValue(201);  // true
        System.out.println("contains201 = " + contains201);

        /**
         * remove(Object rowKey, Object columnKey)：
         * 删除Table中指定行列值的映射关系
         */
        Integer res = table.remove("A", 2);
        System.out.println("res = " + res);

        /**
         * get(Object rowKey, Object columnKey)：
         * 获取Table中指定行列值的映射关系
         */
        Integer mapping = table.get("B", 2);
        System.out.println("mapping = " + mapping);

        /**
         * column(C columnKey)：返回指定columnKey下的所有rowKey与value映射
         */
        Map<String, Integer> columnMap = table.column(2);
        System.out.println("columnMap = " + columnMap);

        /**
         * row(R rowKey)：返回指定rowKey下的所有columnKey与value映射
         */
        Map<Integer, Integer> rowMap = table.row("B");
        System.out.println("rowMap = " + rowMap);

        /**
         * 返回以Table.Cell<R, C, V>为元素的Set集合
         * 类似于Map.entrySet
         */
        Set<Table.Cell<String, Integer, Integer>> cells = table.cellSet();
        for (Table.Cell<String, Integer, Integer> cell : cells) {
            // 获取cell的行值rowKey
            String rowKey = cell.getRowKey();
            System.out.println("rowKey = " + rowKey);
            // 获取cell的列值columnKey
            Integer columnKey = cell.getColumnKey();
            System.out.println("columnKey = " + columnKey);
            // 获取cell的值value
            Integer value = cell.getValue();
            System.out.println("value = " + value);
        }
    }
}
