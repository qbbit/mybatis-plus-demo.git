package com.qbb.mybatisplus;

import java.util.List;

public class PersonV2s {
    private Integer id;
    private List<String> names;

    public PersonV2s(Integer id, List<String> names) {
        this.id = id;
        this.names = names;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<String> getNames() {
        return names;
    }

    public void setNames(List<String> names) {
        this.names = names;
    }

    @Override
    public String toString() {
        return "PersonV2s{" +
                "id=" + id +
                ", names=" + names +
                '}';
    }
}