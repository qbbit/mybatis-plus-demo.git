package com.qbb.mybatisplus.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 学生与课程关系表
 *
 * @TableName tb_stu_sub_relation
 */
@TableName(value = "tb_stu_sub_relation")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StuSubRelation implements Serializable {
    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 学号ID
     */
    private Long stuId;

    /**
     * 学生ID
     */
    private Long subId;

    /**
     * 分数
     */
    private Integer score;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}