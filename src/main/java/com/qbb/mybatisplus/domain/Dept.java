package com.qbb.mybatisplus.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;


/**
 * 部门
 *
 * @TableName tb_dept
 */

/**
 * @author QIUQIU&LL (个人博客:https://www.cnblogs.com/qbbit)
 * @date 2022-11-18  21:48
 * @tags 我爱的人在很远的地方, 我必须更加努力
 * @TableName tb_dept 部门
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "tb_dept")
public class Dept implements Serializable {
    /**
     * 主键ID（全局唯一）
     */
    @TableId
    private Long deptId;

    /**
     * 部门名称
     */
    private String deptName;

    /**
     * 员工
     */
    private Integer staff;

    /**
     * 联系电话
     */
    private String tel;

    /**
     * 逻辑删除（0:未删除；1:已删除）
     */
    @TableLogic
    private Boolean deleted;

    /**
     * 乐观锁
     */
    @Version
    private Integer version;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date gmtCreate;

    /**
     * 修改时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date gmtModified;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    /**
     * 特殊构造器，方便后期对象转换使用
     */
    public Dept(Dept dept) {
        if (Objects.nonNull(dept)) {
            this.deptId = dept.getDeptId();
            this.deptName = dept.getDeptName();
            this.staff = dept.getStaff();
            this.tel = dept.getTel();
            this.deleted = dept.getDeleted();
            this.version = dept.getVersion();
            this.gmtCreate = dept.getGmtCreate();
            this.gmtModified = dept.getGmtModified();
        }
    }
}