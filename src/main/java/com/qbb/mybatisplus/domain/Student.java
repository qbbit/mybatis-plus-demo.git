package com.qbb.mybatisplus.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Objects;

/**
 * 学生表
 *
 * @TableName tb_student
 */
@TableName(value = "tb_student")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Student implements Serializable {
    /**
     * 学号ID
     */
    @TableId(type = IdType.AUTO)
    private Long stuId;

    /**
     * 姓名
     */
    private String stuName;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    /**
     * 特殊构造器，方便后期对象转换使用
     */
    public Student(Student student) {
        if (Objects.nonNull(student)) {
            this.stuId = student.stuId;
            this.stuName = student.stuName;
        }
    }
}