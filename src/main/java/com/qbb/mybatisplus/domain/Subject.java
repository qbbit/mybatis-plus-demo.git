package com.qbb.mybatisplus.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Objects;

/**
 * 课程表
 *
 * @TableName tb_subject
 */
@TableName(value = "tb_subject")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Subject implements Serializable {
    /**
     * 课程ID
     */
    @TableId(type = IdType.AUTO)
    private Long subId;

    /**
     * 课程名
     */
    private String subName;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    /**
     * 特殊构造器，方便后期对象转换使用
     */
    public Subject(Subject subject) {
        if (Objects.nonNull(subject)) {
            this.subId = subject.subId;
            this.subName = subject.subName;
        }
    }
}