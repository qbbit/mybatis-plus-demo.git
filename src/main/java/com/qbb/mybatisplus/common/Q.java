package com.qbb.mybatisplus.common;

import org.springframework.http.HttpStatus;

import java.util.LinkedHashMap;
import java.util.Objects;

/**
 * @author QIUQIU&LL (个人博客:https://www.cnblogs.com/qbbit)
 * @date 2022-11-18  21:48
 * @tags 我爱的人在很远的地方, 我必须更加努力
 */
public class Q extends LinkedHashMap<String, Object> {
    private static final long serialVersionUID = 1L;
    public static final String CODE_TAG = "code";
    public static final String MSG_TAG = "msg";
    public static final String DATA_TAG = "data";

    public Q() {
    }

    public Q(int code, String msg) {
        super.put("code", code);
        super.put("msg", msg);
    }

    public Q(int code, String msg, Object data) {
        super.put("code", code);
        super.put("msg", msg);
        if (Objects.nonNull(data)) {
            super.put("data", data);
        }

    }

    public static Q success() {
        return success("操作成功");
    }

    public static Q success(Object data) {
        return success("操作成功", data);
    }

    public static Q success(String msg) {
        return success(msg, (Object) null);
    }

    public static Q success(String msg, Object data) {
        return new Q(HttpStatus.OK.value(), msg, data);
    }

    public static Q error() {
        return error("操作失败");
    }

    public static Q error(String msg) {
        return error(msg, (Object) null);
    }

    public static Q error(String msg, Object data) {
        return new Q(HttpStatus.INTERNAL_SERVER_ERROR.value(), msg, data);
    }

    public static Q error(int code, String msg) {
        return new Q(code, msg, (Object) null);
    }
}