package com.qbb.mybatisplus.common;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * @author QIUQIU&LL (个人博客:https://www.cnblogs.com/qbbit)
 * @date 2022-11-18  21:48
 * @tags 我爱的人在很远的地方, 我必须更加努力
 */
public class PageEntity {
    private long pageNum = 1L;
    private long pageSize = 10L;

    public PageEntity() {
    }

    public PageEntity(long pageNum, long pageSize) {
        this.pageNum = pageNum;
        this.pageSize = pageSize;
    }

    public long getPageNum() {
        return this.pageNum;
    }

    public void setPageNum(long pageNum) {
        this.pageNum = pageNum;
    }

    public long getPageSize() {
        return this.pageSize;
    }

    public void setPageSize(long pageSize) {
        this.pageSize = pageSize;
    }

    public <T> Page<T> toPage() {
        return new Page(this.pageNum, this.pageSize);
    }

    public <T> Page<T> toPage(Class<T> clazz) {
        return new Page(this.pageNum, this.pageSize);
    }

    public String toString() {
        return "PageEntity{pageNum=" + this.pageNum + ", pageSize=" + this.pageSize + '}';
    }
}
