package com.qbb.mybatisplus.mapper;

import com.qbb.mybatisplus.domain.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author QIUQIU&LL (个人博客:https://www.cnblogs.com/qbbit)
 * @date 2022-11-18  21:48
 * @tags 我爱的人在很远的地方, 我必须更加努力
 */
public interface UserMapper extends BaseMapper<User> {

}




