package com.qbb.mybatisplus.controller;

import com.qbb.mybatisplus.utils.BeanCopyUtils;
import com.qbb.mybatisplus.common.Q;
import com.qbb.mybatisplus.convert.UserConvert;
import com.qbb.mybatisplus.domain.User;
import com.qbb.mybatisplus.entity.vo.UserVO;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author QIUQIU&LL (个人博客:https://www.cnblogs.com/qbbit)
 * @date 2022-11-18  21:48
 * @tags 我爱的人在很远的地方, 我必须更加努力
 */
@Api(tags = "对象Copy测试")
@RestController
@RequestMapping("/copy")
public class CommonController {

    @Operation(summary = "测试-----MapStruct")
    @GetMapping("/mapstruce")
    public Q mapstruce() {
        LocalDateTime startTime = LocalDateTime.now();
        // 需要监控的代码
        for (int i = 0; i < 100000; i++) {
            User user = new User();
            user.setUserId(Long.parseLong(i + ""));
            user.setUserName("qiuqiu" + i);
            user.setAge(i);
            user.setEmail("qiuqiu@qq.com");
            user.setDeptId(Long.parseLong(i % 100 + ""));
            user.setDeleted(false);
            user.setVersion(1);
            user.setGmtCreate(new Date());
            user.setGmtModified(new Date());
            UserVO userVO = UserConvert.CONVERT.userToUserVO(user);
            System.out.println("userVO = " + userVO);
        }
        LocalDateTime endTime = LocalDateTime.now();
        Duration duration = Duration.between(startTime, endTime);
        System.out.println("共耗: " + duration.toHours() + " 小时, " + duration.toMinutes() + " 分钟, "
                + duration.getSeconds() + " 秒, " + duration.toMillis() + " 毫秒");
        return Q.success("共耗: " + duration.toHours() + " 小时, " + duration.toMinutes() + " 分钟, "
                + duration.getSeconds() + " 秒, " + duration.toMillis() + " 毫秒");
    }

    @Operation(summary = "测试-----BeanCopyUtils")
    @GetMapping("/beanCopyUtils")
    public Q beanCopyUtils() {
        LocalDateTime startTime = LocalDateTime.now();
        // 需要监控的代码
        for (int i = 0; i < 100000; i++) {
            User user = new User();
            user.setUserId(Long.parseLong(i + ""));
            user.setUserName("qiuqiu" + i);
            user.setAge(i);
            user.setEmail("qiuqiu@qq.com");
            user.setDeptId(Long.parseLong(i % 100 + ""));
            user.setDeleted(false);
            user.setVersion(1);
            user.setGmtCreate(new Date());
            user.setGmtModified(new Date());
            UserVO userVO = BeanCopyUtils.copyProperties(user, UserVO.class);
            System.out.println("userVO = " + userVO);
        }
        LocalDateTime endTime = LocalDateTime.now();
        Duration duration = Duration.between(startTime, endTime);
        System.out.println("共耗: " + duration.toHours() + " 小时, " + duration.toMinutes() + " 分钟, "
                + duration.getSeconds() + " 秒, " + duration.toMillis() + " 毫秒");
        return Q.success("共耗: " + duration.toHours() + " 小时, " + duration.toMinutes() + " 分钟, "
                + duration.getSeconds() + " 秒, " + duration.toMillis() + " 毫秒");
    }

    @Operation(summary = "测试-----Spring的BeanUtils")
    @GetMapping("/beanUtils")
    public Q beanUtils() {
        LocalDateTime startTime = LocalDateTime.now();
        // 需要监控的代码
        for (int i = 0; i < 100000; i++) {
            User user = new User();
            user.setUserId(Long.parseLong(i + ""));
            user.setUserName("qiuqiu" + i);
            user.setAge(i);
            user.setEmail("qiuqiu@qq.com");
            user.setDeptId(Long.parseLong(i % 100 + ""));
            user.setDeleted(false);
            user.setVersion(1);
            user.setGmtCreate(new Date());
            user.setGmtModified(new Date());
            UserVO userVO = new UserVO();
            BeanUtils.copyProperties(user, userVO);
            System.out.println("userVO = " + userVO);
        }
        LocalDateTime endTime = LocalDateTime.now();
        Duration duration = Duration.between(startTime, endTime);
        System.out.println("共耗: " + duration.toHours() + " 小时, " + duration.toMinutes() + " 分钟, "
                + duration.getSeconds() + " 秒, " + duration.toMillis() + " 毫秒");
        return Q.success("共耗: " + duration.toHours() + " 小时, " + duration.toMinutes() + " 分钟, "
                + duration.getSeconds() + " 秒, " + duration.toMillis() + " 毫秒");
    }
}
