package com.qbb.mybatisplus.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.qbb.mybatisplus.common.PageEntity;
import com.qbb.mybatisplus.common.Q;
import com.qbb.mybatisplus.domain.User;
import com.qbb.mybatisplus.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

/**
 * @author QIUQIU&LL (个人博客:https://www.cnblogs.com/qbbit)
 * @date 2022-11-18  21:48
 * @tags 我爱的人在很远的地方, 我必须更加努力
 */
@Api(tags = "用户一对一查询")
@Slf4j
@RestController
@RequestMapping("/user/one_to_one")
public class OneToOneUserController {

    @Autowired
    private UserService userService;

    @Operation(summary = "单表查询-----用户列表")
    @GetMapping("/list")
    public Q list(User user) {
        return Q.success(userService.list(Wrappers.query(user)));
    }

    @Operation(summary = "单表查询-----用户列表page")
    @GetMapping("/page")
    public Q page(PageEntity pageEntity, User user) {
        return Q.success(userService.page(pageEntity.toPage(), Wrappers.lambdaQuery(user)));
    }

    @Operation(summary = "单表查询-----新增")
    @PostMapping("/add")
    public Q add(@RequestBody User user) {
        return Q.success(userService.save(user));
    }

    @Operation(summary = "单表查询-----修改")
    @PutMapping("/edit")
    public Q edit(@RequestBody User user) {
        return Q.success(userService.updateById(user));
    }

    @Operation(summary = "单表查询-----批量删除")
    @DeleteMapping("/batchRemove")
    public Q batchRemove(@Parameter(name = "ids", description = "用户ID列表") @RequestBody Long[] ids) {
        return Q.success(userService.removeBatchByIds(Arrays.asList(ids)));
    }

    @Operation(summary = "单表查询-----根据ID查询")
    @GetMapping("/detail/{id}")
    public Q detail(@Parameter(name = "id", description = "用户ID") @PathVariable("id") Long id) {
        return Q.success(userService.getById(id));
    }

    @Operation(summary = "一对一查询-----根据ID查询UserVO", description = "一个用户对应一个部门")
    @GetMapping("/vo/detail/{id}")
    public Q voDetail(@Parameter(name = "id", description = "用户ID") @PathVariable("id") Long id) {
        return Q.success(userService.voDetail(id));
    }

    @Operation(summary = "一对一查询-----用户列表分页UserVOPage", description = "一个用户对应一个部门")
    @GetMapping("/vo/page")
    public Q voPage(PageEntity page, User user) {
        return Q.success(userService.voPage(page, user));
    }

    @Operation(summary = "一对一查询-----用户列表UserVOList", description = "一个用户对应一个部门")
    @GetMapping("/vo/list")
    public Q voList(User user) {
        return Q.success(userService.voList(user));
    }

    @Operation(summary = "一对一查询-----根据ID查询UserVO -- easy写法", description = "一个用户对应一个部门 -- easy写法")
    @GetMapping("/easy/vo/detail/{id}")
    public Q voDetailEasy(@Parameter(name = "id", description = "用户ID") @PathVariable("id") Long id) {
        return Q.success(userService.voDetailEasy(id));
    }

    @Operation(summary = "一对一查询-----用户列表分页UserVOPage -- easy写法", description = "一个用户对应一个部门 -- easy写法")
    @GetMapping("/easy/vo/page")
    public Q voPageEasy(PageEntity page, User user) {
        return Q.success(userService.voPageEasy(page, user));
    }

    @Operation(summary = "一对一查询-----用户列表UserVOList -- easy写法", description = "一个用户对应一个部门 -- easy写法")
    @GetMapping("/easy/vo/list")
    public Q voListEasy(User user) {
        return Q.success(userService.voListEasy(user));
    }
}
