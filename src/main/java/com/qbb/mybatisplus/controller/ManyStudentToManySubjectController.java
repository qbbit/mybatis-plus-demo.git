package com.qbb.mybatisplus.controller;

import com.qbb.mybatisplus.common.PageEntity;
import com.qbb.mybatisplus.common.Q;
import com.qbb.mybatisplus.domain.Student;
import com.qbb.mybatisplus.domain.Subject;
import com.qbb.mybatisplus.service.StuSubRelationService;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author QIUQIU&LL (个人博客:https://www.cnblogs.com/qbbit)
 * @date 2022-11-18  21:48
 * @tags 我爱的人在很远的地方, 我必须更加努力
 */
@Api(tags = "学生和课程多对多查询")
@RequestMapping("/many-to-many")
@RestController
public class ManyStudentToManySubjectController {

    @Autowired
    private StuSubRelationService stuSubRelationService;

    // =======================================================根据学生信息查询对应课程信息 START=======================================================

    /**
     * 根据学生ID查询学生信息，并且查询此学生的课程信息
     *
     * @param id 学生ID
     * @return 一条记录
     */
    @Operation(summary = "多对多查询 ----- 根据学生ID查询学生信息，并且查询此学生的课程信息 -- easy写法", description = "一个学生对应多个课程 -- easy写法 -- 单方面来看其实就是一对多")
    @GetMapping("/stuToSub/vo/detail/{id}")
    public Q voDetailEasyStuManyToSubMany(@Parameter(name = "id", description = "学生ID") @PathVariable("id") Long id) {
        return Q.success(stuSubRelationService.voDetailEasyStuManyToSubMany(id));
    }


    /**
     * 根据查询条件查询学生信息列表，并且查询出对应学生的课程信息
     *
     * @param student 查询条件
     * @return
     */
    @Operation(summary = "多对多查询 ----- 根据查询条件查询学生信息列表，并且查询出对应学生的课程信息 -- easy写法", description = "一个学生对应多个课程 -- easy写法 -- 单方面来看其实就是一对多")
    @GetMapping("/stuToSub/vo/list")
    public Q voListEasyStuManyToSubMany(Student student) {
        return Q.success(stuSubRelationService.voListEasyStuManyToSubMany(student));
    }


    /**
     * 根据查询条件查询学生信息分页列表，并且查询出对应学生的课程信息
     *
     * @param page    分页信息
     * @param student 查询条件
     * @return
     */
    @Operation(summary = "多对多查询 ----- 根据查询条件查询学生信息分页列表，并且查询出对应学生的课程信息 -- easy写法", description = "一个学生对应多个课程 -- easy写法 -- 单方面来看其实就是一对多")
    @GetMapping("/stuToSub/vo/page")
    public Q voPageEasyStuManyToSubMany(PageEntity page, Student student) {
        return Q.success(stuSubRelationService.voPageEasyStuManyToSubMany(page.toPage(), student));
    }

    // =======================================================根据学生信息查询对应课程信息 END=======================================================


    // =======================================================根据课程信息查询对应学生信息 START=======================================================

    /**
     * 根据课程ID查询课程信息，并且查询此课程的学生信息
     *
     * @param id 课程ID
     * @return 一条记录
     */
    @Operation(summary = "多对多查询 ----- 根据学生ID查询学生信息，并且查询此学生的课程信息 -- easy写法", description = "一个学生对应多个课程 -- easy写法 -- 单方面来看其实就是一对多")
    @GetMapping("/subToStu/vo/detail/{id}")
    public Q voDetailEasySubManyToStuMany(@Parameter(name = "id", description = "学生ID") @PathVariable("id") Long id) {
        return Q.success(stuSubRelationService.voDetailEasySubManyToStuMany(id));
    }

    /**
     * 根据查询条件查询学生信息列表，并且查询出对应学生的课程信息
     *
     * @param subject 查询条件
     * @return
     */
    @Operation(summary = "多对多查询 ----- 根据查询条件查询学生信息列表，并且查询出对应学生的课程信息 -- easy写法", description = "一个学生对应多个课程 -- easy写法 -- 单方面来看其实就是一对多")
    @GetMapping("/subToStu/vo/list")
    public Q voListEasySubManyToStuMany(Subject subject) {
        return Q.success(stuSubRelationService.voListEasySubManyToStuMany(subject));
    }

    /**
     * 根据查询条件查询学生信息分页列表，并且查询出对应学生的课程信息
     *
     * @param page    分页信息
     * @param subject 查询条件
     * @return
     */
    @Operation(summary = "多对多查询 ----- 根据查询条件查询学生信息分页列表，并且查询出对应学生的课程信息 -- easy写法", description = "一个学生对应多个课程 -- easy写法 -- 单方面来看其实就是一对多")
    @GetMapping("/subToStu/vo/page")
    public Q voPageEasySubManyToStuMany(PageEntity page, Subject subject) {
        return Q.success(stuSubRelationService.voPageEasySubManyToStuMany(page.toPage(), subject));
    }

    // =======================================================根据学生信息查询对应课程信息 END=======================================================

}
