package com.qbb.mybatisplus.controller;

import com.qbb.mybatisplus.common.PageEntity;
import com.qbb.mybatisplus.common.Q;
import com.qbb.mybatisplus.domain.Dept;
import com.qbb.mybatisplus.service.DeptService;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author QIUQIU&LL (个人博客:https://www.cnblogs.com/qbbit)
 * @date 2022-11-18  21:48
 * @tags 我爱的人在很远的地方, 我必须更加努力
 */
@Api(tags = "部门一对多查询")
@RestController
@RequestMapping("/dept/one-to-many")
public class OneToManyDeptController {

    @Autowired
    private DeptService deptService;

    @Operation(summary = "一对多查询-----根据ID查询DeptVO -- easy写法", description = "一个部门对应多个用户 -- easy写法")
    @GetMapping("/easy/vo/detail/{id}")
    public Q voDetailEasyOneToMany(@Parameter(name = "id", description = "部门ID") @PathVariable("id") Long id) {
        return Q.success(deptService.voDetailEasyOneToMany(id));
    }

    @Operation(summary = "一对多查询-----部门列表分页DeptVOPage -- easy写法", description = "一个部门对应多个用户 -- easy写法")
    @GetMapping("/easy/vo/page")
    public Q voPageEasyOneToMany(PageEntity page, Dept dept) {
        return Q.success(deptService.voPageEasyOneToMany(page, dept));
    }

    @Operation(summary = "一对多查询-----部门列表DeptVOList -- easy写法", description = "一个部门对应多个用户 -- easy写法")
    @GetMapping("/easy/vo/list")
    public Q voListEasyOneToMany(Dept dept) {
        return Q.success(deptService.voListEasyOneToMany(dept));
    }
}
