package com.qbb.mybatisplus.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qbb.mybatisplus.utils.EntityUtils;
import com.qbb.mybatisplus.common.PageEntity;
import com.qbb.mybatisplus.domain.Dept;
import com.qbb.mybatisplus.domain.User;
import com.qbb.mybatisplus.entity.vo.DeptVO;
import com.qbb.mybatisplus.mapper.DeptMapper;
import com.qbb.mybatisplus.service.DeptService;
import com.qbb.mybatisplus.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.qbb.mybatisplus.convert.DeptConvert.CONVERT;

/**
 * @author QIUQIU&LL (个人博客:https://www.cnblogs.com/qbbit)
 * @date 2022-11-18  21:48
 * @tags 我爱的人在很远的地方, 我必须更加努力
 */
@Service
public class DeptServiceImpl extends ServiceImpl<DeptMapper, Dept>
        implements DeptService {

    @Autowired
    private UserService userService;

    /**
     * 单个对象：一个部门对应多个用户
     *
     * @param id
     * @return
     */
    @Override
    public DeptVO voDetailEasyOneToMany(Long id) {
        // 先查询部门信息
        Dept dept = this.getById(id);
        // 把部门信息转换成deptVO
        DeptVO deptVO = Optional.ofNullable(dept).map(CONVERT::deptToDeptVO).orElse(null);
        // 填充部门的员工信息
        Optional.ofNullable(deptVO).ifPresent(this::addUserInfo);
        return deptVO;
    }

    private void addUserInfo(DeptVO deptVO) {
        // 根据部门ID查询用户信息
        List<User> userList = userService.list(Wrappers.lambdaQuery(User.class).eq(User::getDeptId, deptVO.getDeptId()));
        Optional.ofNullable(userList).ifPresent(deptVO::setUserList);
    }


    /**
     * 分页查询：一个部门对应多个用户
     *
     * @param page
     * @param dept
     * @return
     */
    @Override
    public IPage<DeptVO> voPageEasyOneToMany(PageEntity page, Dept dept) {
        IPage<DeptVO> toPage = EntityUtils.toPage(page(page.toPage(Dept.class), Wrappers.lambdaQuery(dept)), DeptVO::new);
        if (toPage.getRecords().size() > 0) {
            addUserInfo(toPage);
        }
        return toPage;
    }

    private void addUserInfo(IPage<DeptVO> toPage) {
        // 查询部门对应的多个员工
        Set<Long> deptIds = EntityUtils.toSet(toPage.getRecords(), DeptVO::getDeptId);
        // 用批量deptId查询用户信息
        List<User> userList = userService.list(Wrappers.lambdaQuery(User.class).in(User::getDeptId, deptIds));
        // 使用stream流对用户信息进行分组 Map<deptId,用户列表>，方便后面根据deptId直接获取用户列表信息
        Map<Long, List<User>> listMap = userList.parallelStream().collect(Collectors.groupingBy(User::getDeptId));
        // 方式一:
        // toPage.getRecords().forEach(e -> e.setUserList(listMap.get(e.getDeptId())));
        // 方式二:
        toPage.convert(deptVO -> {
            deptVO.setUserList(listMap.get(deptVO.getDeptId()));
            return deptVO;
        });
    }

    /**
     * 列表查询：一个部门对应多个用户
     *
     * @param dept
     * @return
     */
    @Override
    public List<DeptVO> voListEasyOneToMany(Dept dept) {
        // 查询出部门列表
        List<DeptVO> toList = EntityUtils.toList(list(Wrappers.lambdaQuery(dept)), DeptVO::new);
        Set<Long> deptIds = EntityUtils.toSet(toList, DeptVO::getDeptId);
        List<User> userList = userService.list(Wrappers.lambdaQuery(User.class).in(User::getDeptId, deptIds));
        Map<Long, List<User>> listMap = userList.parallelStream().collect(Collectors.groupingBy(User::getDeptId));
        toList.forEach(e -> e.setUserList(listMap.get(e.getDeptId())));
        return toList;
    }
}




