package com.qbb.mybatisplus.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qbb.mybatisplus.common.PageEntity;
import com.qbb.mybatisplus.domain.Dept;
import com.qbb.mybatisplus.domain.User;
import com.qbb.mybatisplus.entity.vo.UserVO;
import com.qbb.mybatisplus.mapper.UserMapper;
import com.qbb.mybatisplus.service.DeptService;
import com.qbb.mybatisplus.service.UserService;
import com.qbb.mybatisplus.utils.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.qbb.mybatisplus.convert.DeptConvert.CONVERT;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;

/**
 * @author QIUQIU&LL (个人博客:https://www.cnblogs.com/qbbit)
 * @date 2022-11-18  21:48
 * @tags 我爱的人在很远的地方, 我必须更加努力
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User>
        implements UserService {

    @Autowired
    private DeptService deptService;

    /*===========================一对一查询START===============================*/
    @Override
    public UserVO voDetail(Long id) {
        // 查询用户信息
        User user = this.getById(id);
        // 转换为vo对象
        UserVO userVO = Optional.ofNullable(user).map(UserVO::new).orElse(null);
        // 组装数据
        Optional.ofNullable(userVO).ifPresent(this::addDeptNameInfo);
        return userVO;
    }

    private void addDeptNameInfo(UserVO userVO) {
        Dept dept = deptService.getOne(Wrappers.<Dept>lambdaQuery().eq(Dept::getDeptId, userVO.getDeptId()));
        Optional.ofNullable(dept).ifPresent(d -> userVO.setDeptName(d.getDeptName()));
    }

    @Override
    public IPage<UserVO> voPage(PageEntity page, User user) {
        // 先根据条件查询用户列表
        IPage<User> userPage = this.page(page.toPage(), Wrappers.lambdaQuery(user));
        // 初始化Vo
        IPage<UserVO> userVoPage = userPage.convert(UserVO::new);
        if (userVoPage.getRecords().size() > 0) {
            addDeptNameInfo(userVoPage);
        }
        return userVoPage;
    }

    private void addDeptNameInfo(IPage<UserVO> userVoPage) {
        // 提取用户userId，方便批量查询
        Set<Long> deptIds = userVoPage.getRecords().stream().map(User::getDeptId).collect(toSet());
        // 根据deptId查询deptName
        List<Dept> dept = deptService.list(Wrappers.lambdaQuery(Dept.class).in(Dept::getDeptId, deptIds));
        // 构造映射关系，方便匹配deptId与deptName
        Map<Long, String> hashMap = dept.stream().collect(toMap(Dept::getDeptId, Dept::getDeptName));
        // 将查询补充的信息添加到Vo中
        userVoPage.convert(e -> {
            e.setDeptName(hashMap.get(e.getDeptId()));
            return e;
        });
    }

    @Override
    public List<UserVO> voList(User user) {
        List<User> userList = this.list(Wrappers.lambdaQuery(user));
        List<UserVO> userVOList = userList.stream().map(UserVO::new).collect(Collectors.toList());
        // 此步骤可以有多个
        addDeptNameInfo(userVOList);
        return userVOList;
    }

    private void addDeptNameInfo(List<UserVO> userVOList) {
        // 提取userVOList中的deptId集合
        Set<Long> deptIds = userVOList.stream().map(UserVO::getDeptId).collect(toSet());
        // 根据in(id1,id2,id3...)查询
        List<Dept> depts = deptService.list(Wrappers.<Dept>lambdaQuery().select(Dept::getDeptId, Dept::getDeptName).in(Dept::getDeptId, deptIds));
        // 构造映射关系，方便匹配deptId与deptName
        Map<Long, String> hashMap = depts.stream().collect(toMap(Dept::getDeptId, Dept::getDeptName));
        // 封装Vo，并添加到集合中(关键内容)
        userVOList.forEach(e -> e.setDeptName(hashMap.get(e.getDeptId())));
    }

    // =====================================   easy写法   =========================================

    @Override
    public UserVO voDetailEasy(Long id) {
        UserVO userVO = EntityUtils.toObj(this.getById(id), UserVO::new);
        Dept dept = deptService.getById(userVO.getDeptId());
        return CONVERT.appendAttrToUserVO(dept, userVO);
    }

    @Override
    public IPage<UserVO> voPageEasy(PageEntity page, User user) {
        IPage<UserVO> toPage = EntityUtils.toPage(page(page.toPage(User.class), Wrappers.lambdaQuery(user)), UserVO::new);
        Set<Long> deptIds = EntityUtils.toSet(toPage.getRecords(), UserVO::getDeptId);
        Map<Long, Dept> deptMap = EntityUtils.toMap(deptService.listByIds(deptIds), Dept::getDeptId, e -> e);
        toPage.convert(e -> CONVERT.appendAttrToUserVO(deptMap.get(e.getDeptId()), e));
        return toPage;
    }

    @Override
    public List<UserVO> voListEasy(User user) {
        List<UserVO> userVOList = EntityUtils.toList(list(Wrappers.lambdaQuery(user)), UserVO::new);
        Set<Long> deptIds = EntityUtils.toSet(userVOList, UserVO::getDeptId);
        Map<Long, Dept> deptMap = EntityUtils.toMap(deptService.listByIds(deptIds), Dept::getDeptId, e -> e);
        userVOList = userVOList.parallelStream().map(e -> CONVERT.appendAttrToUserVO(deptMap.get(e.getDeptId()), e)).collect(Collectors.toList());
        return userVOList;
    }

    /*===========================一对一查询END===============================*/

}




