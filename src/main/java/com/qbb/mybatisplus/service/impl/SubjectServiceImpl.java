package com.qbb.mybatisplus.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qbb.mybatisplus.domain.Subject;
import com.qbb.mybatisplus.service.SubjectService;
import com.qbb.mybatisplus.mapper.SubjectMapper;
import org.springframework.stereotype.Service;

/**
 * @author QIUQIU&LL (个人博客:https://www.cnblogs.com/qbbit)
 * @date 2022-11-18  21:48
 * @tags 我爱的人在很远的地方, 我必须更加努力
 */
@Service
public class SubjectServiceImpl extends ServiceImpl<SubjectMapper, Subject>
    implements SubjectService{

}




