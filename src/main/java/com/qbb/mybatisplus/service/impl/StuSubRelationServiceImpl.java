package com.qbb.mybatisplus.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qbb.mybatisplus.convert.StuSubRelationConvert;
import com.qbb.mybatisplus.convert.StudentConvert;
import com.qbb.mybatisplus.convert.SubjectConvert;
import com.qbb.mybatisplus.domain.StuSubRelation;
import com.qbb.mybatisplus.domain.Student;
import com.qbb.mybatisplus.domain.Subject;
import com.qbb.mybatisplus.entity.bo.StudentBO;
import com.qbb.mybatisplus.entity.bo.SubjectBO;
import com.qbb.mybatisplus.entity.vo.StudentVO;
import com.qbb.mybatisplus.entity.vo.SubjectVO;
import com.qbb.mybatisplus.mapper.StuSubRelationMapper;
import com.qbb.mybatisplus.service.StuSubRelationService;
import com.qbb.mybatisplus.service.StudentService;
import com.qbb.mybatisplus.service.SubjectService;
import com.qbb.mybatisplus.utils.EntityUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * @author QIUQIU&LL (个人博客:https://www.cnblogs.com/qbbit)
 * @date 2022-11-18  21:48
 * @tags 我爱的人在很远的地方, 我必须更加努力
 */
@Service
public class StuSubRelationServiceImpl extends ServiceImpl<StuSubRelationMapper, StuSubRelation>
        implements StuSubRelationService {

    @Autowired
    private SubjectService subjectService;

    @Autowired
    private StudentService studentService;


    // =======================================================根据学生信息查询对应课程信息 START=======================================================

    /**
     * 根据学生ID查询学生信息，并且查询此学生的课程信息
     *
     * @param id
     * @return
     */
    @Override
    public StudentVO voDetailEasyStuManyToSubMany(Long id) {
        // 先查询学生信息
        Student student = studentService.getById(id);
        // 把学生信息转换成学生BO
        StudentVO studentVO = EntityUtils.toObj(student, StudentVO::new);
        // 查询匹配的关系
        List<StuSubRelation> stuSubRelationList = this.list(Wrappers.<StuSubRelation>lambdaQuery().eq(StuSubRelation::getStuId, id));
        // 获取课程ID列表
        Set<Long> subIds = EntityUtils.toSet(stuSubRelationList, StuSubRelation::getSubId);
        if (Objects.nonNull(studentVO) && !subIds.isEmpty()) {
            // 根据课程ID列表查询课程信息
            List<Subject> subjectList = subjectService.list(Wrappers.<Subject>lambdaQuery().in(Subject::getSubId, subIds));
            // domain ---> BO
            List<SubjectBO> subjectBOList = SubjectConvert.CONVERT.subListToSubBOList(subjectList);
            // 转换为table对象  table对象可以理解为是一个双键Map  ===>  Map<rowKey,Map<columnKey,value>>
            // Table<Long, Long, StuSubRelation> table = TableUtils.createHashTable(stuSubRelationList, StuSubRelation::getStuId, StuSubRelation::getSubId);
            subjectBOList.forEach(StuSubRelationConvert.CONVERT::stuSubRelationToSubjectBO);
            studentVO.setSubjectBOList(subjectBOList);
        }
        return studentVO;
    }

    /**
     * 根据查询条件查询学生信息列表，并且查询出对应学生的课程信息
     *
     * @param student 查询条件
     * @return
     */
    @Override
    public List<StudentVO> voListEasyStuManyToSubMany(Student student) {
        // 根据条件查询学生信息列表
        List<Student> studentList = studentService.list(Wrappers.lambdaQuery(student));
        // student ---> studentVO
        List<StudentVO> studentVOList = EntityUtils.toList(studentList, StudentVO::new);
        // 提取studentIds
        Set<Long> studentIds = EntityUtils.toSet(studentVOList, Student::getStuId);
        if (studentIds.size() == 0) {
            return studentVOList;
        }
        // 根据studentIds去中间表中查询对应关系列表
        List<StuSubRelation> stuSubRelations = list(Wrappers.<StuSubRelation>lambdaQuery().in(StuSubRelation::getStuId, studentIds));
        // 提取subjectIds
        Set<Long> subjectIds = EntityUtils.toSet(stuSubRelations, StuSubRelation::getSubId);
        if (subjectIds.size() == 0) {
            return studentVOList;
        }
        /**
         * 分组转换：
         *     源数据：
         *          1 - 1
         *          1 - 2
         *          2 - 1
         *          2 - 3
         *     转换后：
         *          1 - [1,2]
         *          2 - [1,3]
         */
        Map<Long, List<Long>> map = stuSubRelations.stream().collect(Collectors.groupingBy(StuSubRelation::getStuId,
                Collectors.mapping(StuSubRelation::getSubId, Collectors.toList())));
        // 转换为table对象  table对象可以理解为是一个双键Map  ===>  Map<rowKey,Map<columnKey,value>> 详细的使用参考：https://www.cnblogs.com/qbbit/p/16965597.html
        // Table<Long, Long, StuSubRelation> table = TableUtils.createHashTable(stuSubRelations, StuSubRelation::getStuId, StuSubRelation::getSubId);
        // 根据subjectIds查询课程信息列表
        List<Subject> subjectList = subjectService.list(Wrappers.<Subject>lambdaQuery().in(Subject::getSubId, subjectIds));
        // 使用MapStruct进行高效数据转换
        List<SubjectBO> subjectBOList = SubjectConvert.CONVERT.subListToSubBOList(subjectList);
        // 设置课程信息
        studentVOList.forEach(studentAction(map, subjectBOList));
        return studentVOList;
    }

    /**
     * 根据查询条件查询学生信息分页列表，并且查询出对应学生的课程信息
     *
     * @param page    分页信息
     * @param student 查询条件
     * @return
     */
    @Override
    public IPage<StudentVO> voPageEasyStuManyToSubMany(IPage<Student> page, Student student) {
        IPage<Student> studentIPage = studentService.page(page, Wrappers.lambdaQuery(student));
        IPage<StudentVO> studentVOPage = EntityUtils.toPage(studentIPage, StudentVO::new);
        Set<Long> studentIds = EntityUtils.toSet(studentVOPage.getRecords(), Student::getStuId);
        if (studentIds.size() == 0) {
            return studentVOPage;
        }
        List<StuSubRelation> stuSubRelations = list(Wrappers.lambdaQuery(StuSubRelation.class).in(StuSubRelation::getStuId, studentIds));
        Set<Long> subjectIds = EntityUtils.toSet(stuSubRelations, StuSubRelation::getSubId);
        if (subjectIds.size() == 0) {
            return studentVOPage;
        }
        Map<Long, List<Long>> map = stuSubRelations.stream().collect(Collectors.groupingBy(StuSubRelation::getStuId, Collectors.mapping(StuSubRelation::getSubId, Collectors.toList())));
        List<Subject> subjectList = subjectService.list(Wrappers.lambdaQuery(Subject.class).in(Subject::getSubId, subjectIds));
        List<SubjectBO> subjectBOList = SubjectConvert.CONVERT.subListToSubBOList(subjectList);
        studentVOPage.getRecords().forEach(studentAction(map, subjectBOList));
        return studentVOPage;
    }

    @NotNull
    private static Consumer<StudentVO> studentAction(Map<Long, List<Long>> map, List<SubjectBO> subjectBOList) {
        return studentVO -> {
            List<SubjectBO> list = subjectBOList.stream().filter(e -> map.get(studentVO.getStuId()) != null && map.get(studentVO.getStuId()).contains(e.getSubId())).collect(Collectors.toList());
            list.forEach(StuSubRelationConvert.CONVERT::stuSubRelationToSubjectBO);
            studentVO.setSubjectBOList(list);
        };
    }

    // =======================================================根据学生信息查询对应课程信息 END=======================================================

    // =======================================================根据课程信息查询对应学生信息 START=======================================================

    /**
     * 根据课程ID查询课程信息，并且查询此课程的学生信息
     *
     * @param id 课程ID
     * @return 一条记录
     */
    @Override
    public SubjectVO voDetailEasySubManyToStuMany(Long id) {
        // 根据课程ID查询课程信息
        Subject subject = subjectService.getById(id);
        // 将subject转换为subjectVO
        SubjectVO subjectVO = EntityUtils.toObj(subject, SubjectVO::new);
        // 查询匹配关系
        List<StuSubRelation> stuSubRelationList = this.list(Wrappers.<StuSubRelation>lambdaQuery().eq(StuSubRelation::getSubId, id));
        // 获取学生ID列表
        Set<Long> stuIds = EntityUtils.toSet(stuSubRelationList, StuSubRelation::getStuId);
        if (Objects.nonNull(subjectVO) && stuIds.size() > 0) {
            // 根据学生ID列表查询学生信息
            List<Student> studentList = studentService.list(Wrappers.<Student>lambdaQuery().in(Student::getStuId, stuIds));
            // domain ---> BO
            List<StudentBO> studentBOList = StudentConvert.CONVERT.stuListToStuBOList(studentList);
            studentBOList.forEach(StuSubRelationConvert.CONVERT::stuSubRelationToStudentBO);
            subjectVO.setStudentBOList(studentBOList);
        }
        return subjectVO;
    }

    /**
     * 根据查询条件查询学生信息列表，并且查询出对应学生的课程信息
     *
     * @param subject 查询条件
     * @return
     */
    @Override
    public List<SubjectVO> voListEasySubManyToStuMany(Subject subject) {
        // 根据条件查询课程列表
        List<Subject> subjectList = subjectService.list(Wrappers.lambdaQuery(subject));
        // subject ---> subjectVO
        List<SubjectVO> subjectVOList = EntityUtils.toList(subjectList, SubjectVO::new);
        // 提取subjectIds
        Set<Long> subjectIds = EntityUtils.toSet(subjectVOList, Subject::getSubId);
        if (subjectIds.size() == 0) {
            return subjectVOList;
        }
        // 提取subjectIds去中间表查询对应关系列表
        List<StuSubRelation> stuSubRelationList = list(Wrappers.<StuSubRelation>lambdaQuery().in(StuSubRelation::getSubId, subjectIds));
        // 提取studentIds
        Set<Long> studentIds = EntityUtils.toSet(stuSubRelationList, StuSubRelation::getStuId);
        if (studentIds.size() == 0) {
            return subjectVOList;
        }
        Map<Long, List<Long>> map = stuSubRelationList.stream().collect(Collectors.groupingBy(StuSubRelation::getStuId, Collectors.mapping(StuSubRelation::getSubId, Collectors.toList())));
        List<Student> studentList = studentService.list(Wrappers.<Student>lambdaQuery().in(Student::getStuId, studentIds));
        List<StudentBO> studentBOList = StudentConvert.CONVERT.stuListToStuBOList(studentList);
        subjectVOList.forEach(subjectAction(map, studentBOList));
        return subjectVOList;
    }

    /**
     * 根据查询条件查询学生信息分页列表，并且查询出对应学生的课程信息
     *
     * @param page    分页信息
     * @param subject 查询条件
     * @return
     */
    @Override
    public IPage<SubjectVO> voPageEasySubManyToStuMany(Page<Subject> page, Subject subject) {
        IPage<SubjectVO> subjectVOPage = EntityUtils.toPage(subjectService.page(page, Wrappers.lambdaQuery(subject)), SubjectVO::new);
        Set<Long> subjectIds = EntityUtils.toSet(subjectVOPage.getRecords(), Subject::getSubId);
        if (subjectIds.size() == 0) {
            return subjectVOPage;
        }
        List<StuSubRelation> stuSubRelations = list(Wrappers.lambdaQuery(StuSubRelation.class).in(StuSubRelation::getSubId, subjectIds));
        Set<Long> studentIds = EntityUtils.toSet(stuSubRelations, StuSubRelation::getStuId);
        if (studentIds.size() == 0) {
            return subjectVOPage;
        }
        Map<Long, List<Long>> map = stuSubRelations.stream().collect(Collectors.groupingBy(StuSubRelation::getSubId, Collectors.mapping(StuSubRelation::getStuId, Collectors.toList())));
        List<Student> studentList = studentService.list(Wrappers.lambdaQuery(Student.class).in(Student::getStuId, studentIds));
        List<StudentBO> studentBOList = StudentConvert.CONVERT.stuListToStuBOList(studentList);
        subjectVOPage.getRecords().forEach(subjectAction(map, studentBOList));
        return subjectVOPage;
    }

    @NotNull
    private static Consumer<SubjectVO> subjectAction(Map<Long, List<Long>> map, List<StudentBO> studentBOList) {
        return (subjectVO) -> {
            List<StudentBO> list = studentBOList.stream().filter(e -> map.get(subjectVO.getSubId()) != null && map.get(subjectVO.getSubId()).contains(e.getStuId())).collect(Collectors.toList());
            list.forEach(StuSubRelationConvert.CONVERT::stuSubRelationToStudentBO);
            subjectVO.setStudentBOList(list);
        };
    }
    // =======================================================根据学生信息查询对应课程信息 END=======================================================
}




