package com.qbb.mybatisplus.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.qbb.mybatisplus.common.PageEntity;
import com.qbb.mybatisplus.domain.User;
import com.qbb.mybatisplus.entity.vo.UserVO;

import java.util.List;

/**
 * @author QIUQIU&LL (个人博客:https://www.cnblogs.com/qbbit)
 * @date 2022-11-18  21:48
 * @tags 我爱的人在很远的地方, 我必须更加努力
 */
public interface UserService extends IService<User> {

    /*===========================一对一查询START===============================*/

    UserVO voDetail(Long id);

    IPage<UserVO> voPage(PageEntity page, User user);

    List<UserVO> voList(User user);

    // 简单查询
    UserVO voDetailEasy(Long id);

    IPage<UserVO> voPageEasy(PageEntity page, User user);

    List<UserVO> voListEasy(User user);

    /*===========================一对一查询END===============================*/

}
