package com.qbb.mybatisplus.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.qbb.mybatisplus.domain.StuSubRelation;
import com.qbb.mybatisplus.domain.Student;
import com.qbb.mybatisplus.domain.Subject;
import com.qbb.mybatisplus.entity.vo.StudentVO;
import com.qbb.mybatisplus.entity.vo.SubjectVO;

import java.util.List;

/**
 * @author QIUQIU&LL (个人博客:https://www.cnblogs.com/qbbit)
 * @date 2022-11-18  21:48
 * @tags 我爱的人在很远的地方, 我必须更加努力
 */
public interface StuSubRelationService extends IService<StuSubRelation> {

    // =======================================================根据学生信息查询对应课程信息 START=======================================================

    /**
     * 多对多查询单个对象
     *
     * @param id
     * @return
     */
    StudentVO voDetailEasyStuManyToSubMany(Long id);

    /**
     * 多对多列表查询
     *
     * @param student
     * @return
     */
    List<StudentVO> voListEasyStuManyToSubMany(Student student);

    /**
     * 多对多分页查询
     *
     * @param page
     * @param student
     * @return
     */
    IPage<StudentVO> voPageEasyStuManyToSubMany(IPage<Student> page, Student student);


    // =======================================================根据学生信息查询对应课程信息 END=======================================================


    // =======================================================根据课程信息查询对应学生信息 START=======================================================

    /**
     * 根据课程ID查询课程信息，并且查询此课程的学生信息
     *
     * @param id 课程ID
     * @return 一条记录
     */
    SubjectVO voDetailEasySubManyToStuMany(Long id);

    /**
     * 根据查询条件查询学生信息列表，并且查询出对应学生的课程信息
     *
     * @param subject
     * @return
     */
    List<SubjectVO> voListEasySubManyToStuMany(Subject subject);

    /**
     * 根据查询条件查询学生信息分页列表，并且查询出对应学生的课程信息
     *
     * @param page    分页信息
     * @param subject 查询条件
     * @return
     */
    IPage<SubjectVO> voPageEasySubManyToStuMany(Page<Subject> page, Subject subject);

    // =======================================================根据学生信息查询对应课程信息 END=======================================================
}
