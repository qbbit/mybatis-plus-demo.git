package com.qbb.mybatisplus.service;

import com.qbb.mybatisplus.domain.Subject;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author QIUQIU&LL (个人博客:https://www.cnblogs.com/qbbit)
 * @date 2022-11-18  21:48
 * @tags 我爱的人在很远的地方, 我必须更加努力
 */
public interface SubjectService extends IService<Subject> {

}
