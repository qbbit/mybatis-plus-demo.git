package com.qbb.mybatisplus.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.qbb.mybatisplus.common.PageEntity;
import com.qbb.mybatisplus.domain.Dept;
import com.baomidou.mybatisplus.extension.service.IService;
import com.qbb.mybatisplus.entity.vo.DeptVO;

import java.util.List;

/**
 * @author QIUQIU&LL (个人博客:https://www.cnblogs.com/qbbit)
 * @date 2022-11-18  21:48
 * @tags 我爱的人在很远的地方, 我必须更加努力
 */
public interface DeptService extends IService<Dept> {

    /*===========================一对多查询START===============================*/

    DeptVO voDetailEasyOneToMany(Long id);

    IPage<DeptVO> voPageEasyOneToMany(PageEntity page, Dept dept);

    List<DeptVO> voListEasyOneToMany(Dept dept);

    /*===========================一对多查询END===============================*/

}
