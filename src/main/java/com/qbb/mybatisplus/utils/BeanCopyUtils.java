package com.qbb.mybatisplus.utils;

import com.baomidou.mybatisplus.core.toolkit.ReflectionKit;
import org.springframework.beans.BeanUtils;

import java.lang.reflect.Field;
import java.util.*;

/**
 * @author QIUQIU&LL (个人博客:https://www.cnblogs.com/qbbit)
 * @date 2022-11-18  21:48
 * @tags 我爱的人在很远的地方, 我必须更加努力
 */
public class BeanCopyUtils extends BeanUtils {
    public BeanCopyUtils() {
    }

    public static void copyProperties(Object source, Object target) {
        if (source != null && target != null) {
            List<Field> fieldList = ReflectionKit.getFieldList(target.getClass());
            Set<String> set = new HashSet();

            try {
                Iterator var4 = fieldList.iterator();

                while (var4.hasNext()) {
                    Field field = (Field) var4.next();
                    field.setAccessible(true);
                    Optional.ofNullable(field.get(target)).ifPresent((e) -> {
                        set.add(field.getName());
                    });
                }
            } catch (IllegalAccessException var6) {
                var6.printStackTrace();
            }

            String[] ignoreProperties = (String[]) set.toArray(new String[0]);
            BeanUtils.copyProperties(source, target, ignoreProperties);
        }

    }

    public static <T> T copyProperties(Object source, Class<T> clazz) {
        try {
            T target = clazz.newInstance();
            BeanUtils.copyProperties(source, target);
            return target;
        } catch (IllegalAccessException | InstantiationException var3) {
            var3.printStackTrace();
            return null;
        }
    }
}