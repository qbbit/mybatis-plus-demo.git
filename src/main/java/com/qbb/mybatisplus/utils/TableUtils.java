package com.qbb.mybatisplus.utils;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

import java.util.Objects;
import java.util.function.Function;

/**
 * @author QIUQIU&LL (个人博客:https://www.cnblogs.com/qbbit)
 * @date 2022-11-18  21:48
 * @tags 我爱的人在很远的地方, 我必须更加努力
 */
public class TableUtils {
    public TableUtils() {
    }

    public static <T, R, C, V> Table<R, C, V> createHashTable(Iterable<T> source, Function<? super T, ? extends R> r, Function<? super T, ? extends C> c, Function<? super T, ? extends V> v) {
        Objects.requireNonNull(source);
        Objects.requireNonNull(r);
        Objects.requireNonNull(c);
        Objects.requireNonNull(v);
        Table<R, C, V> table = HashBasedTable.create();
        source.forEach((e) -> table.put(EntityUtils.toObj(e, r), EntityUtils.toObj(e, c), EntityUtils.toObj(e, v)));
        return table;
    }

    public static <R, C, T> Table<R, C, T> createHashTable(Iterable<T> source, Function<? super T, ? extends R> r, Function<? super T, ? extends C> c) {
        return createHashTable(source, r, c, e -> e);
    }
}