package com.qbb.mybatisplus.convert;

import com.qbb.mybatisplus.domain.Subject;
import com.qbb.mybatisplus.entity.bo.SubjectBO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author QIUQIU&LL (个人博客:https://www.cnblogs.com/qbbit)
 * @date 2022-11-18  21:48
 * @tags 我爱的人在很远的地方, 我必须更加努力
 */
@Mapper
public interface SubjectConvert {

    SubjectConvert CONVERT = Mappers.getMapper(SubjectConvert.class);

    SubjectBO subToSubBO(Subject subject);

    /**
     * 批量转换
     */
    @Mappings({
            @Mapping(source = "subId", target = "subId"),
            @Mapping(source = "subName", target = "subName")
    })
    List<SubjectBO> subListToSubBOList(List<Subject> subjectList);

}
