package com.qbb.mybatisplus.convert;

import com.qbb.mybatisplus.domain.StuSubRelation;
import com.qbb.mybatisplus.entity.bo.StudentBO;
import com.qbb.mybatisplus.entity.bo.SubjectBO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

/**
 * @author QIUQIU&LL (个人博客:https://www.cnblogs.com/qbbit)
 * @date 2022-11-18  21:48
 * @tags 我爱的人在很远的地方, 我必须更加努力
 */
@Mapper
public interface StuSubRelationConvert {

    StuSubRelationConvert CONVERT = Mappers.getMapper(StuSubRelationConvert.class);

    /**
     * StuSubRelation ---> StudentBO
     *
     * @param studentBO
     * @return
     */
    @Mapping(source = "stuId", target = "stuId")
    StuSubRelation stuSubRelationToStudentBO(StudentBO studentBO);


    @Mapping(source = "subId", target = "subId")
    StuSubRelation stuSubRelationToSubjectBO(SubjectBO subjectBO);

}
