package com.qbb.mybatisplus.convert;

import com.qbb.mybatisplus.domain.Dept;
import com.qbb.mybatisplus.entity.vo.DeptVO;
import com.qbb.mybatisplus.entity.vo.UserVO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

/**
 * @author QIUQIU&LL (个人博客:https://www.cnblogs.com/qbbit)
 * @date 2022-11-18  21:48
 * @tags 我爱的人在很远的地方, 我必须更加努力
 */
@Mapper
public interface DeptConvert {

    DeptConvert CONVERT = Mappers.getMapper(DeptConvert.class);


    /**
     * 默认转换
     *
     * @param dept
     * @return
     */
    /*@BeanMapping(ignoreByDefault = true)
    @Mappings({
            @Mapping(source = "deptId", target = "deptId"),
            @Mapping(source = "deptName", target = "deptName"),
            @Mapping(source = "staff", target = "staff"),
            @Mapping(source = "tel", target = "tel")
    })
    UserVO deptToUserVO(Dept dept);*/

    /**
     * 追加更新属性
     * 可以有返回值也可以无返回值
     *
     * @param userVO 源对象
     * @param dept
     */
    UserVO appendAttrToUserVO(Dept dept, @MappingTarget UserVO userVO);

    /**
     * dept转换deptVO
     *
     * @param dept
     * @return
     */
    DeptVO deptToDeptVO(Dept dept);

}
