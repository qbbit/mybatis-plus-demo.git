package com.qbb.mybatisplus.convert;

import com.qbb.mybatisplus.domain.Student;
import com.qbb.mybatisplus.entity.bo.StudentBO;
import com.qbb.mybatisplus.entity.vo.StudentVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author QIUQIU&LL (个人博客:https://www.cnblogs.com/qbbit)
 * @date 2022-11-18  21:48
 * @tags 我爱的人在很远的地方, 我必须更加努力
 */
@Mapper
public interface StudentConvert {

    StudentConvert CONVERT = Mappers.getMapper(StudentConvert.class);

    StudentVO stuToStuVO(Student student);

    /**
     * 批量转换
     */
    @Mappings({
            @Mapping(source = "stuId", target = "stuId"),
            @Mapping(source = "stuName", target = "stuName")
    })
    List<StudentBO> stuListToStuBOList(List<Student> studentList);
}
