package com.qbb.mybatisplus.convert;

import com.qbb.mybatisplus.domain.User;
import com.qbb.mybatisplus.entity.vo.UserVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

/**
 * @author QIUQIU&LL (个人博客:https://www.cnblogs.com/qbbit)
 * @date 2022-11-18  21:48
 * @tags 我爱的人在很远的地方, 我必须更加努力
 */
@Mapper
public interface UserConvert {

    UserConvert CONVERT = Mappers.getMapper(UserConvert.class);

    @Mappings({
            @Mapping(source = "userId", target = "userId"),
            @Mapping(source = "userName", target = "userName")
    })
    UserVO userToUserVO(User user);
}
