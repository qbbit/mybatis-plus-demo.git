package com.qbb.mybatisplus.entity.bo;

import lombok.Data;

/**
 * @author QIUQIU&LL (个人博客:https://www.cnblogs.com/qbbit)
 * @date 2022-11-18  21:48
 * @tags 我爱的人在很远的地方, 我必须更加努力
 */
@Data
public class SubjectBO {

    /**
     * 课程ID
     */
    private Long subId;

    /**
     * 课程名
     */
    private String subName;

}
