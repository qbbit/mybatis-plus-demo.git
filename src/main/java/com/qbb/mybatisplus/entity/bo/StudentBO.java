package com.qbb.mybatisplus.entity.bo;

import lombok.Data;

/**
 * @author QIUQIU&LL (个人博客:https://www.cnblogs.com/qbbit)
 * @date 2022-11-18  21:48
 * @tags 我爱的人在很远的地方, 我必须更加努力
 */
@Data
public class StudentBO {

    /**
     * 学号ID
     */
    private Long stuId;

    /**
     * 姓名
     */
    private String stuName;
}
