package com.qbb.mybatisplus.entity.vo;

import com.qbb.mybatisplus.domain.Student;
import com.qbb.mybatisplus.entity.bo.SubjectBO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author QIUQIU&LL (个人博客:https://www.cnblogs.com/qbbit)
 * @date 2022-11-18  21:48
 * @tags 我爱的人在很远的地方, 我必须更加努力
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StudentVO extends Student {
    private List<SubjectBO> subjectBOList;

    public StudentVO(Student student) {
        super(student);
    }
}
