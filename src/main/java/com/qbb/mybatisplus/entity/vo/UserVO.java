package com.qbb.mybatisplus.entity.vo;

import com.qbb.mybatisplus.domain.User;
import lombok.Data;

/**
 * @author QIUQIU&LL (个人博客:https://www.cnblogs.com/qbbit)
 * @date 2022-11-18  21:48
 * @tags 我爱的人在很远的地方, 我必须更加努力
 */
@Data
public class UserVO extends User {
    private String deptName;
    private Integer staff;
    private String tel;

    public UserVO() {
    }

    public UserVO(User user) {
        super(user);
    }

    @Override
    public String toString() {
        return "UserVO{" +
                "deptName='" + deptName + '\'' +
                ", staff=" + staff +
                ", tel='" + tel + '\'' +
                "} " + super.toString();
    }
}
