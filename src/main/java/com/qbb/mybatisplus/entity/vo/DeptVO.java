package com.qbb.mybatisplus.entity.vo;

import com.qbb.mybatisplus.domain.Dept;
import com.qbb.mybatisplus.domain.User;
import lombok.Data;

import java.util.List;

/**
 * @author QIUQIU&LL (个人博客:https://www.cnblogs.com/qbbit)
 * @date 2022-11-18  21:48
 * @tags 我爱的人在很远的地方, 我必须更加努力
 */
@Data
public class DeptVO extends Dept {
    private static final long serialVersionUID = 1L;
    private List<User> userList;

    public DeptVO() {
    }

    public DeptVO(Dept dept) {
        super(dept);
    }
}
