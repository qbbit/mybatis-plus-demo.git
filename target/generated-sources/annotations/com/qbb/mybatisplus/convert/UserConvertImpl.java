package com.qbb.mybatisplus.convert;

import com.qbb.mybatisplus.domain.User;
import com.qbb.mybatisplus.entity.vo.UserVO;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-12-08T16:30:58+0800",
    comments = "version: 1.5.3.Final, compiler: javac, environment: Java 17.0.2 (Azul Systems, Inc.)"
)
public class UserConvertImpl implements UserConvert {

    @Override
    public UserVO userToUserVO(User user) {
        if ( user == null ) {
            return null;
        }

        UserVO userVO = new UserVO();

        userVO.setUserId( user.getUserId() );
        userVO.setUserName( user.getUserName() );
        userVO.setAge( user.getAge() );
        userVO.setEmail( user.getEmail() );
        userVO.setDeptId( user.getDeptId() );
        userVO.setDeleted( user.getDeleted() );
        userVO.setVersion( user.getVersion() );
        userVO.setGmtCreate( user.getGmtCreate() );
        userVO.setGmtModified( user.getGmtModified() );

        return userVO;
    }
}
