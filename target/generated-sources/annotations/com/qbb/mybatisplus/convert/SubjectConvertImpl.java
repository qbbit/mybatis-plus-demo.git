package com.qbb.mybatisplus.convert;

import com.qbb.mybatisplus.domain.Subject;
import com.qbb.mybatisplus.entity.bo.SubjectBO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-12-08T16:30:58+0800",
    comments = "version: 1.5.3.Final, compiler: javac, environment: Java 17.0.2 (Azul Systems, Inc.)"
)
public class SubjectConvertImpl implements SubjectConvert {

    @Override
    public SubjectBO subToSubBO(Subject subject) {
        if ( subject == null ) {
            return null;
        }

        SubjectBO subjectBO = new SubjectBO();

        subjectBO.setSubId( subject.getSubId() );
        subjectBO.setSubName( subject.getSubName() );

        return subjectBO;
    }

    @Override
    public List<SubjectBO> subListToSubBOList(List<Subject> subjectList) {
        if ( subjectList == null ) {
            return null;
        }

        List<SubjectBO> list = new ArrayList<SubjectBO>( subjectList.size() );
        for ( Subject subject : subjectList ) {
            list.add( subToSubBO( subject ) );
        }

        return list;
    }
}
