package com.qbb.mybatisplus.convert;

import com.qbb.mybatisplus.domain.Student;
import com.qbb.mybatisplus.entity.bo.StudentBO;
import com.qbb.mybatisplus.entity.vo.StudentVO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-12-08T16:30:58+0800",
    comments = "version: 1.5.3.Final, compiler: javac, environment: Java 17.0.2 (Azul Systems, Inc.)"
)
public class StudentConvertImpl implements StudentConvert {

    @Override
    public StudentVO stuToStuVO(Student student) {
        if ( student == null ) {
            return null;
        }

        StudentVO studentVO = new StudentVO();

        studentVO.setStuId( student.getStuId() );
        studentVO.setStuName( student.getStuName() );

        return studentVO;
    }

    @Override
    public List<StudentBO> stuListToStuBOList(List<Student> studentList) {
        if ( studentList == null ) {
            return null;
        }

        List<StudentBO> list = new ArrayList<StudentBO>( studentList.size() );
        for ( Student student : studentList ) {
            list.add( studentToStudentBO( student ) );
        }

        return list;
    }

    protected StudentBO studentToStudentBO(Student student) {
        if ( student == null ) {
            return null;
        }

        StudentBO studentBO = new StudentBO();

        studentBO.setStuId( student.getStuId() );
        studentBO.setStuName( student.getStuName() );

        return studentBO;
    }
}
