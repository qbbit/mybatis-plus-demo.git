package com.qbb.mybatisplus.convert;

import com.qbb.mybatisplus.domain.StuSubRelation;
import com.qbb.mybatisplus.entity.bo.StudentBO;
import com.qbb.mybatisplus.entity.bo.SubjectBO;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-12-08T16:36:59+0800",
    comments = "version: 1.5.3.Final, compiler: javac, environment: Java 17.0.2 (Azul Systems, Inc.)"
)
public class StuSubRelationConvertImpl implements StuSubRelationConvert {

    @Override
    public StuSubRelation stuSubRelationToStudentBO(StudentBO studentBO) {
        if ( studentBO == null ) {
            return null;
        }

        StuSubRelation stuSubRelation = new StuSubRelation();

        stuSubRelation.setStuId( studentBO.getStuId() );

        return stuSubRelation;
    }

    @Override
    public StuSubRelation stuSubRelationToSubjectBO(SubjectBO subjectBO) {
        if ( subjectBO == null ) {
            return null;
        }

        StuSubRelation stuSubRelation = new StuSubRelation();

        stuSubRelation.setSubId( subjectBO.getSubId() );

        return stuSubRelation;
    }
}
