package com.qbb.mybatisplus.convert;

import com.qbb.mybatisplus.domain.Dept;
import com.qbb.mybatisplus.entity.vo.DeptVO;
import com.qbb.mybatisplus.entity.vo.UserVO;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-12-08T17:03:53+0800",
    comments = "version: 1.5.3.Final, compiler: javac, environment: Java 17.0.2 (Azul Systems, Inc.)"
)
public class DeptConvertImpl implements DeptConvert {

    @Override
    public UserVO appendAttrToUserVO(Dept dept, UserVO userVO) {
        if ( dept == null ) {
            return userVO;
        }

        userVO.setDeptId( dept.getDeptId() );
        userVO.setDeleted( dept.getDeleted() );
        userVO.setVersion( dept.getVersion() );
        userVO.setGmtCreate( dept.getGmtCreate() );
        userVO.setGmtModified( dept.getGmtModified() );
        userVO.setDeptName( dept.getDeptName() );
        userVO.setStaff( dept.getStaff() );
        userVO.setTel( dept.getTel() );

        return userVO;
    }

    @Override
    public DeptVO deptToDeptVO(Dept dept) {
        if ( dept == null ) {
            return null;
        }

        DeptVO deptVO = new DeptVO();

        deptVO.setDeptId( dept.getDeptId() );
        deptVO.setDeptName( dept.getDeptName() );
        deptVO.setStaff( dept.getStaff() );
        deptVO.setTel( dept.getTel() );
        deptVO.setDeleted( dept.getDeleted() );
        deptVO.setVersion( dept.getVersion() );
        deptVO.setGmtCreate( dept.getGmtCreate() );
        deptVO.setGmtModified( dept.getGmtModified() );

        return deptVO;
    }
}
